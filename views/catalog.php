<?php
	require "../partials/template.php";

	function get_body_contents(){
?>

	<h1 class="text-center py-3"> Hi from Catalog</h1>
	<div class="container">
		<div class="row">
		<?php  
			$products = file_get_contents("../assets/lib/products.json") ;
			$products_array = json_decode($products, true);
			foreach ($products_array as $indiv_products){

		?>
		<div class="col-lg-4 py-2 ">
			<div class="card">
				<img class="card-image-top"  height="300px" src="../assets/lib/<?php echo $indiv_products['image']?>" >
			
			<div class="card-body">
				<h5 class="card-title"><?php echo $indiv_products['name'] ?></h5>
				<p class="card-text">Price: <?php echo $indiv_products['price'] ?></p>
				<p class="card-text">Description <?php echo $indiv_products['description'] ?></p>
				
			</div>
			<div class="card-footer text-center">
				<a href="../controllers/delete-item-process.php?name=<?php echo $indiv_products['name'] ?>" class="btn btn-danger">Delete Items</a>
			</div>
			<div class="card-footer">
				<form action="../controllers/add-to-cart-process.php" method="POST">
					<input type="hidden" name="name" class="form-control" value="<?php echo $indiv_products['name'] ?>">
					<input type="number"  name="quantity" value="1" class="form-control">
					<button type="submit" class="btn btn-info btn-block">+ Add to cart</button>
				</form>
			</div>
			</div>
		</div>

		<?php 

			}
		?>
	</div>   
	</div>

<?php
	}

?>