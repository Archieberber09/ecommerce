<!DOCTYPE html>
<html>
<head>
	<title>Store Name</title>

	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/superhero/bootstrap.css">
</head>
<body>	
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="../index.php">Game Center</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarColor03">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="views/catalog.php"> Products <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="views/add-items.php">Add Items</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="views/cart.php">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="vh-100 d-flex justify-content-center align-items-center flex-column">
			<h1>Welcome to Game Center</h1>
			<a href="views/catalog.php" class="btn btn-success">View Games</a>
		</div>
	</header>
			<!-- Featured Product Page -->
	<section>
		<h1 class="text-center p-5">Top Rated Games</h1>
		<div class="container">
			<div class="row">
				<?php 
					$products = file_get_contents("assets/lib/products.json");
					$products_array = json_decode($products, true);
					// var_dump($products_array);
					for($i = 0; $i < 3 ; $i++){
						// var_dump($products_array[$i]);

					
				?>
					<div class="col-lg-4 py-2">
						<div class="card bg-light">
							<img src="assets/lib/<?php echo $products_array[$i]["image"]  ?>" class="card-img-top" height="450px">	
						
							<div class="card-body">
								<h5 class="card-title" ><?php echo $products_array[$i]["name"] ?></h5>
								<p class="card-text">Price: <?php echo $products_array[$i]["price"]?></p>
								<p class="card-text">Description: <?php echo $products_array[$i]["description"]?></p>
							</div>
						</div>
					</div>
				<?php 
				 	}
				?>
				
			</div>
		</div>
	</section>

</body>
</html>